import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:compbuilderapp/routes/RouteName.dart';
import 'Drawer.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff5956e9),
      ),
      drawer: DrawerScreen(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Find the Best \n Hero to Win \n Easily",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Times New Roman',
                ),
                textAlign: TextAlign.center),
            SizedBox(height: 20),
            Text("Stop nge-troll agar kredit skormu \n tidak merothol",
                style: TextStyle(
                  fontFamily: 'Times New Roman',
                  color: Color(0xff82868E),
                ),
                textAlign: TextAlign.center),
            SizedBox(height: 20),
            Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                style: raisedButttonStyle,
                child: Text("Heroes"),
                onPressed: () {
                  Get.toNamed(RouteName.page_4);
                },
              ),
            ),
            SizedBox(height: 20),
            Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                style: raisedButttonStyle,
                child: Text("Items"),
                onPressed: () {
                  Get.toNamed(RouteName.page_5);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButttonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.white,
    primary: Color(0xff4A3FCB),
    minimumSize: Size(88, 36),
    padding: EdgeInsets.symmetric(horizontal: 16),
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10))));
