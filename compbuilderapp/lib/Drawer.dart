import 'package:compbuilderapp/DisplayHero.dart';
import 'package:compbuilderapp/LoginScreen.dart';
import 'package:compbuilderapp/ProfileScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:compbuilderapp/routes/RouteName.dart';

class DrawerScreen extends StatefulWidget {
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  State<DrawerScreen> createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser!.email);
    }
    var x = auth.currentUser!.email.toString();
    return Drawer(
        backgroundColor: Colors.white,
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: Color(0xff5956e9),
              ),
              accountName: Text(x.replaceAll('@gmail.com', '')),
              currentAccountPicture: CircleAvatar(
                  backgroundImage: AssetImage("assets/img/Profile.png")),
              accountEmail: Text(x),
            ),
            DrawerListTile(
              iconData: Icons.contacts_rounded,
              title: "Profile",
              onTilePressed: () {
                Get.toNamed(RouteName.page_6);
              },
            ),
            DrawerListTile(
              iconData: Icons.logout_rounded,
              title: "Log Out",
              onTilePressed: () {
                Get.toNamed(RouteName.page_1);
              },
            ),
          ],
        ));
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTile(
      {Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
        onTap: onTilePressed,
        dense: true,
        leading: Icon(iconData),
        title: Text(
          title!,
          style: TextStyle(fontSize: 16),
        ));
  }
}
