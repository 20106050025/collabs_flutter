import 'dart:ui';

import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:responsive_grid_list/responsive_grid_list.dart';

class DisplayItem extends StatefulWidget {
  const DisplayItem({Key? key}) : super(key: key);

  @override
  State<DisplayItem> createState() => _DisplayItemState();
}

class _DisplayItemState extends State<DisplayItem> {
  Query dbRef = FirebaseDatabase.instance.ref().child('items');
  DatabaseReference reference = FirebaseDatabase.instance.ref().child('items');

Widget listItem({required Map items}) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 100, vertical: 5),
      padding: const EdgeInsets.all(16),
      height: 170,
      width: 60,
      color: Color(0xff5956e9),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.network(items['icon']),
          SizedBox(height: 20),
          Text(items['item_name'], style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w800,
          ),),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("All Items"),
        backgroundColor: Color(0xff5956e9),
      ),
      body: Container(
        height: double.infinity,
        child: FirebaseAnimatedList(
            query: dbRef,
            itemBuilder: (BuildContext context, DataSnapshot snapshot,
                Animation<double> animation, int index) {
              Map items = snapshot.value as Map;
              items['key'] = snapshot.key;
              return listItem(items: items);
            }),
      ),
    );
  }
}
