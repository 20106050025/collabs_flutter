import 'package:compbuilderapp/LoginScreen.dart';
import 'package:compbuilderapp/RegisterScreen.dart';
import 'package:compbuilderapp/HomeScreen.dart';
import 'package:compbuilderapp/DisplayHero.dart';
import 'package:compbuilderapp/DisplayItem.dart';
import 'package:compbuilderapp/ProfileScreen.dart';
import 'package:compbuilderapp/routes/RouteName.dart';
import 'package:get/get.dart';

class pageRouteApp {
  static final pages = [
    GetPage(
      name: RouteName.page_1,
      page: () => LoginScreen(),
    ),
    GetPage(
      name: RouteName.page_2,
      page: () => RegisterScreen(),
    ),
    GetPage(
      name: RouteName.page_3,
      page: () => HomePage(),
    ),
    GetPage(
      name: RouteName.page_4,
      page: () => DisplayHero(),
    ),
    GetPage(
      name: RouteName.page_5,
      page: () => DisplayItem(),
    ),
    GetPage(
      name: RouteName.page_6,
      page: () => ProfileScreen(),
    ),
  ];
}
