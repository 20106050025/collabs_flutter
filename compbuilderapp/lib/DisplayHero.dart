import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class DisplayHero extends StatefulWidget {
  const DisplayHero({Key? key}) : super(key: key);

  @override
  State<DisplayHero> createState() => _DisplayHeroState();
}

class _DisplayHeroState extends State<DisplayHero> {
  Query dbRef = FirebaseDatabase.instance.ref().child('heroes');
  DatabaseReference reference = FirebaseDatabase.instance.ref().child('heroes');

  Widget listItem({required Map heroes}) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 100, vertical: 5),
      padding: const EdgeInsets.all(16),
      height: 170,
      width: 60,
      color: Color(0xff5956e9),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.network(heroes['portrait']),
          SizedBox(height: 10),
          Text(heroes['hero_name'], style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w800
          )),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("All Hero"),
        backgroundColor: Color(0xff5956e9),
      ),
      body: Container(
        height: double.infinity,
        child: FirebaseAnimatedList(
            query: dbRef,
            itemBuilder: (BuildContext context, DataSnapshot snapshot,
                Animation<double> animation, int index) {
              Map heroes = snapshot.value as Map;
              heroes['key'] = snapshot.key;
              return listItem(heroes: heroes);
            }),
      ),
    );
  }
}
